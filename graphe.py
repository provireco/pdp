import StringIO,os;
import file;

class Graph:
	def get_sommets(self):
		return self.sommet.keys()

	def get_nb_sommets():
		return len( self.get_sommets() )

	def __str__(self):
		return str(self.Type) + " : " + str( self.sommet )

	def get_adjacent(self, u):
		return self.sommet[u]
	
	def Inverser(G):
		Ginv=OrientedGraph()
		for key in G.nom.keys():
			dep=G.nom[key][1]
			arr=G.nom[key][0]
			Ginv.AjouterArrete(key,dep,arr)
		return Ginv

class OrientedGraph(Graph):
	def __init__(self,nom={},sommet={}):
		self.nom=nom#dico d'arrete cle arrete valeurs sommets
		self.sommet=sommet#liste d'adjacence(dictionnaire)
		self.Type="oriented"

	def creationDOT(self):
		fichier = open("Oriented.dot","w")
		fichier.write("digraph G{")
		for key in self.nom.keys():
			fichier.write(str(self.nom[key][0]))
			fichier.write("->")
			fichier.write(str(self.nom[key][1]))
			fichier.write(";")
			fichier.write("\n")
		fichier.write("}")
		fichier.close()
		os.system("dotty Oriented.dot &")

	def ajouter_sommet( self, sommet ):
		if not sommet in self.sommet:
			self.sommet[ sommet ] = []

	def AjouterArrete(self, nomArrete, depart, arrivee, poids=0 ):
		self.ajouter_sommet( depart )
		self.ajouter_sommet( arrivee )
		self.sommet[ depart ].append( arrivee )
		self.nom[nomArrete]= [ depart, arrivee, poids ]

class NonOrientedGraph(Graph):
	def __init__(self,nom={},sommet={}):
		self.nom=nom
		self.sommet=sommet
		self.Type="Non_oriented"

	def creationDOT(self):
		fichier = open("NonOriented.dot","w")
		fichier.write("digraph G{")
		for key in self.nom.keys():
			fichier.write(str(self.nom[key][0]))
			fichier.write("->")
			fichier.write(str(self.nom[key][1]))
			fichier.write("[dir=\"none\"];")
			fichier.write("\n")
		fichier.write("}")
		fichier.close()
		fichier.close()
		os.system("dotty NonOriented.dot &")

	def ajouter_sommet( self, sommet ):
		if not sommet in self.sommet:
			self.sommet[ sommet ] = []

	def AjouterArrete(self, nomArrete, depart, arrivee, poids=0):
		self.ajouter_sommet( depart )
		self.ajouter_sommet( arrivee )
		self.sommet[ depart ].append( arrivee )
		self.nom[nomArrete]= [ depart, arrivee, poids ]

def afficheradjacence(graphe):
	for key in graphe.sommet:
		print key, ": ",graphe.sommet[key]

class Kruskal:
	def __init__(self,G):
		self.G=G
		self.foret=[]
		self.tripoids=[]#liste des arrete triee par poids croissant
		self.ensemble=[]
		self.triCroissant()
		self.ensembleK()
		self.get_foret()

	def get_poids(self):
		return self.tripoids

	def get_foret(self):
		print "foret",self.foret
		return self.foret

	def triCroissant(self):
		for key in self.G.nom.keys():
			self.tripoids.append(self.G.nom[key][2]) 
		self.tripoids.sort()
		for key in self.G.nom.keys():
			for i in range (len(self.tripoids)):
				if self.tripoids[i]==self.G.nom[key][2]:
					self.tripoids[i]=key
					break

	def trouver(self,sommet):
		for i in range (len(self.ensemble)):
			for j in self.ensemble[i]:
				if j==sommet:
					return i

	def fusion(self,u,v):
		indiceU=0
		indiceV=0
		for i in range (len(self.ensemble)):
			for j in self.ensemble[i]:
				if j==u:
					indiceU=i
				if j==v:
					indiceV=i
		for i in self.ensemble[indiceV]:
			self.ensemble[indiceU].append(i)
		self.ensemble.pop(indiceV)

	def ensembleK(self):
		for i in self.G.get_sommets():
			temp=[i]
			print "temp",temp
			self.ensemble.append(temp)
		print "ensemble",self.ensemble
		for i in self.tripoids:
			u=self.G.nom[i][0]
			v=self.G.nom[i][1]
			if self.trouver(u)!=self.trouver(v):
				self.foret.append(self.G.nom[i])
				self.fusion(u,v)

class Dijkstra:
	def __init__(self,G):
		self.G=G
		self.d={}
		self.maxi=0
		self.pere={}
		self.Fpriorite=file.nouvelle()#file de poids croissants
		self.TriCroissant()
		self.maximum()
		for i in self.G.get_sommets():
			self.d[i]=self.maxi

	def maximum(self):
		self.maxi=self.G.nom[self.Fpriorite[0]][2]
		for i in range (len(self.Fpriorite)):
			if self.maxi<self.G.nom[self.Fpriorite[i]][2]:
				self.maxi=self.G.nom[self.Fpriorite[i]][2]

	def TriCroissant(self):
		for key in self.G.nom.keys():
			self.Fpriorite.append(self.G.nom[key][2]) 
		self.Fpriorite.sort()
		for key in self.G.nom.keys():
			for i in range (len(self.Fpriorite)):
				if self.Fpriorite[i]==self.G.nom[key][2]:
					self.Fpriorite[i]=key
					break

	#~ def MaJdistance(self,u,v):
		#~ if self.d[u]>(self.d[v]):
			#~ 

	def suiteDijkstra(self,r):
		self.d[r]=0
		print self.d
		print self.Fpriorite
		while not file.est_vide(self.Fpriorite):
			u=file.defiler(self.Fpriorite)#plus petit poids d'arrete de la liste Fpriorite
			print u
			u=self.G.nom[u][0]
			print "u",u
			for v in self.G.sommet[u]:
				print "v",v
				

class ParcoursProfondeur:
	def __init__(self,G):
		self.G = G
		self.couleur = {}
		self.couleur_cfc = {}
		#~ self.cfcnb=0
		#~ self.Mcfc={}
		self.pere = {}
		self.temps = 0
		self.fin={}
		self.debut={}
		self.topo=file.nouvelle()
		self.CFC=file.nouvelle()
		for i in G.get_sommets():
			self.couleur[i]="blanc"
			self.pere[i]=-1
		for sommet in G.get_sommets():
			if self.couleur[sommet]=="blanc":
				self.Visiter_PP(sommet)
		self.topo.reverse()
		
	def ComposantesFortementConnexes(self,liste):
		for i in liste:
			self.couleur_cfc[i]="blanc"
		for sommet in liste:
			if self.couleur_cfc[sommet]=="blanc":
				self.Visiter_CFC(sommet)
	
	def Visiter_CFC(self,sommet):
		#~ self.cfcnb+=1
		#~ self.Mcfc[sommet]=self.cfcnb
		file.enfiler(self.CFC,sommet)# liste des CFC
		self.couleur_cfc[sommet]="gris"
		for i in self.G.get_adjacent(sommet):
			if self.couleur_cfc[i]=="blanc":
				self.Visiter_PP(i)
				self.Visiter_CFC(i)
		self.couleur_cfc[sommet]="noir"

	def Visiter_PP(self,sommet):
		self.couleur[sommet]="gris"
		self.temps+=1
		self.debut[sommet]=self.temps
		for i in self.G.get_adjacent(sommet):
			if self.couleur[i]=="blanc":
				self.pere[i]=sommet
				self.Visiter_PP(i)
		self.couleur[sommet]="noir"
		self.temps+=1
		file.enfiler(self.topo,sommet)# tri topo
		self.fin[sommet]=self.temps

	def get_couleur(self,sommet):
		return self.couleur[sommet]

	def get_cycle(self):
		return self.cycle

	def get_Topo(self):
		return self.topo

	def get_CFC(self):
		return self.CFC

	def get_fin(self):
		return self.fin

def parcoursprofondeur(G):
	PP=ParcoursProfondeur(G)
	for sommet in G.get_sommets():
		print "le sommet "+str(sommet)+" a la couleur "+PP.get_couleur(sommet)

def CFC(G):
	PP=ParcoursProfondeur(G)
	liste=PP.get_Topo()
	GI=G.Inverser()
	PPI=ParcoursProfondeur(GI)
	PPI.ComposantesFortementConnexes(liste)
	print "cfc: ",PPI.get_CFC()

def TriTopologique(G):
	PP=ParcoursProfondeur(G)
	print "file",PP.get_Topo()
	#~ print "debut",PP.debut
	#~ print "fin:",PP.fin

class ParcoursLargeur:
	def __init__(self, G, s):
		self.G = G
		self.origin = s
		self.couleur={}
		self.pere={}
		self.distance={}
		for i in G.get_sommets():
			self.couleur[i]="blanc"
			self.pere[i]=None
			self.distance[i]=None
		self.couleur[s]="gris"
		self.distance[s]=0
		F=file.nouvelle()
		file.enfiler(F,s)
		while not file.est_vide(F):
			u=file.sommetF(F)
			for temp in G.get_adjacent(u):
				if self.couleur[temp]=="blanc":
					self.couleur[temp]="gris"
					self.distance[temp]=self.distance[u]+1
					self.pere[temp]=u
					file.enfiler(F,temp)
			file.defiler(F)
			self.couleur[u]="noir"

	def get_path(self, sommet ):
		if sommet == self.origin:
			return [self.origin]
		if self.pere[sommet] <> None :
			path = self.get_path( self.pere[sommet] )
			return path + [sommet]

	def get_distance(self, sommet ):
		return self.distance[sommet]

def Chemins(G,s):
	PL = ParcoursLargeur(G,s)
	for sommet in G.get_sommets():
		print "la distance entre " + str(s) + " et " + str(sommet) + " est " + str(PL.get_distance(sommet)) + " avec le chemin " + str( PL.get_path( sommet ) )

def Accessible (G,d):
	PL=ParcoursLargeur(G,d)
	for sommet in G.get_sommets():
		for acces in PL.get_path(sommet):
			if acces==sommet:
				print "le sommet "+str(sommet)+" est accessible depuis "+str(d)

def transformationONO(grapheO):
	temp = NonOrientedGraph()
	temp.nom=grapheO.nom
	temp.sommet=grapheO.sommet
	return temp

def afficherarrete(graphe):
	for key in graphe.nom.keys():
		print str(key)+" arrete de: "+str(graphe.nom[key][0])+" a: "+str(graphe.nom[key][1])+" a un poids de: "+str(graphe.nom[key][2])

def affichertaille(graphe):
	nbarrete=0
	nbsommet=0
	for key in graphe.nom.keys():
		nbarrete+=1
	print "il y a "+str(nbarrete)+" arretes"
	for key in graphe.sommet.keys():
		nbsommet+=1
	print "il y a "+str(nbsommet)+" sommets"

print "Dijkstra"
D=OrientedGraph()
D.AjouterArrete( "A1", 's', 'u', 10)
D.AjouterArrete( "A2", 'u', 'v', 1)
D.AjouterArrete( "A3", 'u', 'x', 2)
D.AjouterArrete( "A4", 'x', 'u', 3)
D.AjouterArrete( "A5", 'v', 'y', 4)
D.AjouterArrete( "A6", 'y', 'v', 6)
D.AjouterArrete( "A7", 's', 'x', 5)
D.AjouterArrete( "A8", 'x', 'v', 9)
D.AjouterArrete( "A9", 'y', 's', 7)
D.AjouterArrete( "A10", 'x', 'y', 2)
GD=Dijkstra(D)
GD.suiteDijkstra('s')

#~ print "Non Oriented"
#~ P =  NonOrientedGraph()
#~ P.AjouterArrete( "A1", 0, 1, 1)
#~ P.AjouterArrete( "A2", 0, 2, 3 )
#~ P.AjouterArrete( "A3", 0, 3, 6 )
#~ P.AjouterArrete( "A4", 1, 2, 1 )
#~ P.AjouterArrete( "A5", 1, 5, 4 )
#~ P.AjouterArrete( "A6", 1, 4, 6 )
#~ P.AjouterArrete( "A7", 1, 3, 10 )
#~ P.AjouterArrete( "A8", 2, 5, 6 )
#~ P.AjouterArrete( "A9", 3, 4, 3 )
#~ P.AjouterArrete( "A10", 3, 5, 4 )
#~ P.AjouterArrete( "A11", 4, 5, 2 )
#~ afficherarrete(P)
#~ GK=Kruskal(P)
#~ p=GK.triCroissant()
#~ GK.ensembleK()
#~ print GK.get_poids()
