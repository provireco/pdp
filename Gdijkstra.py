import StringIO,os;
import file;

class Graph:
	def get_sommets(self):
		return self.sommet.keys()

	def get_nb_sommets():
		return len( self.get_sommets() )

	def __str__(self):
		return str(self.Type) + " : " + str( self.sommet )

	def get_adjacent(self, u):#essai git 1
		return self.sommet[u]
	
#essaie git 2
class OrientedGraph(Graph):#essai git 3
	def __init__(self,arrete=[],sommet={}):
		self.arrete=arrete#liste d'arrete [depart,arrivee,poids]
		self.sommet=sommet#liste d'adjacence(dictionnaire)
		self.Type="oriented"

	def creationDOT(self):#essai fit 4
		fichier = open("Oriented.dot","w")
		fichier.write("digraph G{")
		for i in range(len(self.arrete)):
			fichier.write(str(self.arrete[i][0]))
			fichier.write("->")
			fichier.write(str(self.arrete[i][1]))
			fichier.write(";")
			fichier.write("\n")
		fichier.write("}")
		fichier.close()
		os.system("dotty Oriented.dot &")

	def ajouter_sommet( self, sommet ):
		if not sommet in self.sommet:
			self.sommet[ sommet ] = []

	def AjouterArrete(self, depart, arrivee, poids=0 ):
		self.ajouter_sommet( depart )
		self.ajouter_sommet( arrivee )
		self.sommet[ depart ].append( arrivee )
		self.arrete.append([depart, arrivee, poids ])


class Dijkstra:
	def __init__(self,G):
		self.G=G
		self.d={}
		self.maxi=0
		self.pere={}
		self.Fpriorite=file.nouvelle()#file de poids croissants
		self.TriCroissant()
		self.maximum()
		for i in self.G.get_sommets():
			self.d[i]=self.maxi
			self.pere[i]=''
		self.suiteDijkstra('s')

	def maximum(self):
		self.maxi=self.Fpriorite[0][2]
		for i in range (len(self.Fpriorite)):
			if self.maxi<self.Fpriorite[i][2]:
				self.maxi=self.Fpriorite[i][2]

	def TriCroissant(self):
		for i in range (len(self.G.arrete)):
			self.Fpriorite.append(self.G.arrete[i][2])
		self.Fpriorite.sort()
		for j in range (len(self.G.arrete)):
			for i in range (len(self.Fpriorite)):
				if self.Fpriorite[i]==self.G.arrete[j][2]:
					self.Fpriorite[i]=self.G.arrete[j]
					break
		#~ print "prio : ",self.Fpriorite

	def suiteDijkstra(self,r):
		self.d[r]=0
		while not file.est_vide(self.Fpriorite):
			u=file.defiler(self.Fpriorite)#plus petit poids d'arrete de la liste Fpriorite
			for v in self.G.sommet[u[0]]:
				for i in range (len (self.G.arrete)):
					if self.G.arrete[i][0]==u[0] and self.G.arrete[i][1]==v:
						w=self.G.arrete[i][2]
						print "u:",u[0],"v:",v,"w :",w
				if (self.d[u[0]]+w)<self.d[v]:
					self.d[v]=self.d[u[0]]+w
					self.pere[v]=u[0]
		print "d : ",self.d
		print "pere : ", self.pere

	def get_pere(self):
		return self.pere



print "Dijkstra"
D=OrientedGraph()
D.AjouterArrete( 's', 'u', 10)
D.AjouterArrete( 'u', 'v', 1)
D.AjouterArrete( 'u', 'x', 2)
D.AjouterArrete( 'x', 'u', 3)
D.AjouterArrete( 'v', 'y', 4)
D.AjouterArrete( 'y', 'v', 6)
D.AjouterArrete( 's', 'x', 5)
D.AjouterArrete( 'x', 'v', 9)
D.AjouterArrete( 'y', 's', 7)
D.AjouterArrete( 'x', 'y', 2)
GD=Dijkstra(D)
#~ GD.suiteDijkstra('s')
#~ print "pere:"+str(GD.get_pere())
