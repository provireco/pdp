#!/usr/bin/python
# -*- coding: latin-1 -*-

import string
import copy

# valable pour piles et files

def nouvelle():
	return []
	
def est_vide(t):
	return len(t)==0

def copie(t):
	return copy.copy(t)

# primitives de files

def sommetF(t):
	return t[0]

def enfiler(t,e):
	t.append(e)

def defiler(t):
	return t.pop(0)
